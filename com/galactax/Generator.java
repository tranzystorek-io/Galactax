/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.galactax;

import com.badlogic.gdx.math.MathUtils;

/**
 * This class is responsible for calculating time and properties for
 * mass generated objects (asteroids, enemies, background stars).
 * <p>
 * On each delta time update it calls GameScreen's spawn methods
 * with pre-randomized data such as x-y coordinates, sizes etc.
 * 
 * @author marcin
 */
public class Generator {
    
    private final GameScreen workingScreen;
    
    private float smallAsteroidTimer;
    private float mediumAsteroidTimer;
    private float bigAsteroidTimer;
    
    private float starTimer;
    private float enemyTimer;
    
    private float waveTimer;
    private float waitTimer;
    
    private boolean isWaveOngoing;
    
    private WaveType waveType;
    
    private enum WaveType {
	
	ASTEROIDS,
	ENEMIES,
	BOTH {
	    
	    @Override
	    public WaveType next() {
		
		return ASTEROIDS;
	    }
	};
	
	public WaveType next() {
	    
	    return values()[ordinal() + 1];
	}
    }
    
    /**
     * Constructs a generator with a reference to a GameScreen
     * 
     * @param scr GameScreen for which to perform generation
     */
    public Generator(final GameScreen scr) {
	
	this.workingScreen = scr;
	
	smallAsteroidTimer = 0f;
	mediumAsteroidTimer = 0f;
	bigAsteroidTimer = 0f;
	
	starTimer = 0f;
	enemyTimer = 0f;
	
	waveTimer = 0f;
	waitTimer = 0f;
	
	isWaveOngoing = false;
	
	waveType = WaveType.ASTEROIDS;
    }
    
    /**
     * Perform generation based on a passed time delta
     * 
     * @param delta - passed time portion in seconds
     */
    public void generate(float delta) {
	
	updateWaveTimer(delta);
	
	if(updateStarTimer(delta)) {
	    
	    boolean small = MathUtils.random(1) == 1;
	    
	    workingScreen.spawnStar(MathUtils.random(5f,
						     Constants.GAME_WIDTH - (small ? workingScreen.smallStar.getRegionWidth()
										   : workingScreen.bigStar.getRegionWidth()) - 5f),
				    MathUtils.random(Constants.MIN_STAR_SPEED, Constants.MAX_STAR_SPEED),
				    small);
	}
	
	if(isWaveOngoing) {
	    
	    //System.out.println("" + waveType);
	    
	    if(waveType == WaveType.BOTH || waveType == WaveType.ASTEROIDS) {
	    
		boolean[] spawnedSizes = updateAsteroidTimers(delta);

		for(int i = 0; i < spawnedSizes.length; ++i)
		    if(spawnedSizes[i]) {

			workingScreen.spawnAsteroid(MathUtils.random(5f, Constants.GAME_WIDTH - Constants.ASTEROID_SIZES[i] - 5f),
						    Asteroid.Size.getFromInt(i));
		    }
	    }
	    
	    if(waveType == WaveType.BOTH || waveType == WaveType.ENEMIES) {
		
		if(updateEnemyTimer(delta)) {

		    workingScreen.spawnEnemy(MathUtils.random(Constants.ENEMY_MIN_HEIGHT, Constants.ENEMY_MAX_HEIGHT),
					     MathUtils.random(1) == 1);
		}
	    }
	}
    }
    
    private void updateWaveTimer(float delta) {
	
	if(isWaveOngoing) {
	    
	    waveTimer += delta;
	    
	    //current wave has ended
	    if(waveTimer > Constants.WAVE_TIME) {
	    
		waveTimer = 0f;
		
		waveType = waveType.next();
		resetWaveTimers();
		
		isWaveOngoing = false;
	    }
	}
	
	else {
	    
	    waitTimer += delta;
	    
	    //current wave begins
	    if(waitTimer > Constants.WAIT_BETWEEN_WAVES_TIME) {
		
		waitTimer = 0f;
		
		isWaveOngoing = true;
	    }
	}
    }
    
    private boolean updateStarTimer(float delta) {
	
	starTimer += delta;
	
	if(starTimer > Constants.STAR_SPAWN_TIME) {
	    
	    starTimer = 0f;
	    
	    return true;
	}
	
	return false;
    }
    
    private boolean[] updateAsteroidTimers(float delta) {
	
	smallAsteroidTimer += delta;
	mediumAsteroidTimer += delta;
	bigAsteroidTimer += delta;
	
	boolean[] results = {false, false, false};
	
	if(smallAsteroidTimer > Constants.ASTEROID_SPAWN_TIMES[0]) {
	    
	    smallAsteroidTimer = 0f;
	    
	    results[0] = MathUtils.random(5) < 2;
	}
	
	if(mediumAsteroidTimer > Constants.ASTEROID_SPAWN_TIMES[1]) {
	    
	    mediumAsteroidTimer = 0f;
	    
	    results[1] = MathUtils.random(6) < 4;
	}
	
	if(bigAsteroidTimer > Constants.ASTEROID_SPAWN_TIMES[2]) {
	    
	    bigAsteroidTimer = 0f;
	    
	    results[2] = MathUtils.random(3) < 2;
	}
	
	return results;
    }
    
    private boolean updateEnemyTimer(float delta) {
	
	enemyTimer += delta;
	
	if(enemyTimer > Constants.ENEMY_SPAWN_TIME * 1.1) {
	    
	    enemyTimer = 0f;
	    
	    return true;
	}
	
	return false;
    }
    
    private void resetWaveTimers() {
	
	enemyTimer = 0f;
	
	smallAsteroidTimer = 0f;
	mediumAsteroidTimer = 0f;
	bigAsteroidTimer = 0f;
    }
}
